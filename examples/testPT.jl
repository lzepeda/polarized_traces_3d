# loading the functions needed to build the system
include("../src/subdomain2.jl");
include("../src/preconditioners.jl")
include("../src/modelHDG3D.jl")

using IterativeSolvers

# options for the direct solver used within each layer
# Umfpack will be the slowest, and it is only for
# testing porpouses. You may want to install MUMPS
# or if you have a MKL license, use MKLPardiso
UmfpackBool    = false 
MKLPardisoBool = false
MUMPSBool      = true

# loading the different solvers
MKLPardisoBool == true  && using Pardiso
MUMPSBool == true       && using MUMPS

# using some extra optimization to reduce the
# execution time
OptBool = false

if( UmfpackBool )
  solvertype = "UMFPACK"
elseif( MUMPSBool )
  solvertype = "MUMPS"
elseif( MKLPardisoBool )
  solvertype = "MKLPARDISO"
end

# number of deegres of freedom per dimension
nx = 23;
ny = 1;
nz = 23;
npml = 2;
nbuffer = 1;
order = 3;

println("order=$order")

# the number of points have to such
# (nz - 2*npml)/nLayer is an integer

# number of layers
nLayers = 12;

# interior degrees of freedom
# we sample the z space, the solver is normalize such that the
# z legnth of the computational domain is always 1
z = linspace(0,1,nz);


# extra arguments
h     = z[2]-z[1];    # mesh width
fac   = 20.0;  # absorbition coefficient for the PML
K     = nz/2;         # wave number
omega = 2*pi*K;       # frequency

println( "wave number = $K" )

bdy = [0.0, 0.0, 0.0 ]

# bulding the domain decomposition data structure
println("Building the subdomains")

mesh, modelArray = constructLayeredModel( nx, ny, nz, npml, nbuffer, bdy, h, fac, order, omega, nLayers, solvertype )

subDomains = [Subdomain(modelArray[ii],1) for ii=1:nLayers];

f = createGlobalRHS( subDomains );
#uExact = zeros( Complex128, size(system.mM,1) );
#uExact[ system.mFree ] = system.mM[ system.mFree, system.mFree ] \ system.mRhs[ system.mFree ];

#########################################################################
# Solving the local problem
# perform the local solves and extract the traces
uBdyPol = extractRHS(subDomains,f[:]);

# We vectorize the RHS, and put in the correct form
uBdyPer = -vectorizePolarizedBdyDataRHS(subDomains, uBdyPol)

##########################################################################
#  Solving for the boundary data

# allocating the preconditioner
if OptBool == false
    # by default we use the Gauss-Seidel Preconditioner
      Precond = IntegralPreconditioner(subDomains);
    else
        # we can use the optimized Gauss-Seidel that uses the
          # jump conditiones to perform one less local solve per layer
            Precond = IntegralPreconditioner(subDomains,precondtype ="GSOpt");
          end


          ##############  GMRES #####################
          # # solving for the traces

          u = 0*uBdyPer;

          @time data = gmres!(u,x->applyMMOpt2(subDomains,x), uBdyPer, Precond; tol=1e-6,restart=100);

          println("Number of iteration of GMRES : ", countnz( data[2].residuals[:]))



#########################################################################
# testing the solution

# we apply the polarized matrix to u to check for the error
@time MMu = applyMM(subDomains, u);

println("Error for the polarized boundary integral system = ", norm(MMu - uBdyPer)/norm(uBdyPer) );

# adding the polarized traces to obtain the traces
# u = u^{\uparrow} + u^{\downarrow}
uBdySol = u[1:round(Integer,end/2)]+u[(1+round(Integer,end/2)):end];
  
uGamma = -vectorizeBdyData(subDomains, uBdyPol);

Mu = applyM(subDomains, uBdySol);

# checking that we recover the good solution
println("Error for the boundary integral system = ", norm(Mu - uGamma)/norm(uGamma));

########### Reconstruction ###############

#(u0,u1,uN,uNp) = devectorizeBdyDataContiguous(subDomains, uBdySol);

#uVol = reshape(reconstruction(subDomains, f, u0, u1, uN, uNp), nx, ny, nz);

