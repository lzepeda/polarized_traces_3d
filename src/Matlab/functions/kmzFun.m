function res = kmzFunPML(x,y,z)
    C = 1;
    omega = 4*2.1*pi; 
    delta=1;
    a = 0.25;
    b = 0.75;

    kappa = alpha(omega,C,delta,a,b,z)./(alpha(omega,C,delta,a,b,x).*alpha(omega,C,delta,a,b,y));;
    res = 1./kappa;
    %res = (3+1i)*ones(size(x));
end
