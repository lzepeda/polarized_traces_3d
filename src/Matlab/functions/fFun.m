function res = fFun(x,y,z)
    C = 1;
    delta=1;
    a = 0.25;
    b = 0.75;
    omega = 4*2.1*pi;
    omegaPML = omega;
    u = x.*(1-x).*y.*(1-y).*z.*(1-z);
    ux = (1-x).*y.*(1-y).*z.*(1-z) - x.*y.*(1-y).*z.*(1-z);
    uy = (1-y).*x.*(1-x).*z.*(1-z) - y.*x.*(1-x).*z.*(1-z);
    uz = (1-z).*x.*(1-x).*y.*(1-y) - z.*x.*(1-x).*y.*(1-y);
    uxx = -y.*(1-y).*z.*(1-z) - y.*(1-y).*z.*(1-z);
    uyy = -x.*(1-x).*z.*(1-z) - x.*(1-x).*z.*(1-z);
    uzz = -x.*(1-x).*y.*(1-y) - x.*(1-x).*y.*(1-y);
    res = (-alpha(omegaPML,C,delta,a,b,x).*uxx-dalpha(omegaPML,C,delta,a,b,x).*ux).*alpha(omegaPML,C,delta,a,b,x)...
      +(-alpha(omegaPML,C,delta,a,b,y).*uyy-dalpha(omegaPML,C,delta,a,b,y).*uy).*alpha(omegaPML,C,delta,a,b,y)...
      +(-alpha(omegaPML,C,delta,a,b,z).*uzz-dalpha(omegaPML,C,delta,a,b,z).*uz).*alpha(omegaPML,C,delta,a,b,z)...
             -omega^2*mFun(x,y,z).*u;
end
