function res = mFunPML(omega,C,delta,ax,bx,ay,by,az,bz,x,y,z)
    res = mFun(x,y,z) ./ (alpha(omega,C,delta,ax,bx,x).*alpha(omega,C,delta,ay,by,y).*alpha(omega,C,delta,az,bz,z));
end
