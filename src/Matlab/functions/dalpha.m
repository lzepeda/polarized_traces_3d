function res = dalpha(omega,C,delta,a,b,x)
  res = -((1i*dsigma(C,delta,a,b,x)./omega))./(1+1i*sigma(C,delta,a,b,x)).^2;
end

function res = sigma(C,delta,a,b,x)
  res = (C/delta)*(((a-x)./delta).^2.*(x<a)...
        +((x-b)./delta).^2.*(x>b));
end
function res = dsigma(C,delta,a,b,x)
  res = (C/delta)*((-2*(a-x)./delta^2).*(x<a)...
        +(2*(x-b)./delta^2).*(x>b));
end
