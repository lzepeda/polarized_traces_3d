function res = kmxFun(x,y,z)
    C = 1;
    omega = 4*2.1*pi; 
    delta=1;
    a = 0.25;
    b = 0.75;

    kappa = alpha(omega,C,delta,a,b,x)./(alpha(omega,C,delta,a,b,y).*alpha(omega,C,delta,a,b,z));;
    res = 1./kappa;
    %res = (1+1i)*ones(size(x));
end
