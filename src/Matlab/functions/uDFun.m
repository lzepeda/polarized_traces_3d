function res = uDFun(x,y,z)
    res = x.*(1-x).*y.*(1-y).*z.*(1-z);
end