function res = kmxFunPML(omega,C,delta,ax,bx,ay,by,az,bz,x,y,z)
    kappa = alpha(omega,C,delta,ax,bx,x)./(alpha(omega,C,delta,ay,by,y).*alpha(omega,C,delta,az,bz,z));
    res = 1./kappa;
    %res = (1+1i)*ones(size(x));
end
