function res = alpha(omega,C,delta,a,b,x)
  res = 1./(1+1i*sigma(C,delta,a,b,x)./omega);
end

function res = sigma(C,delta,a,b,x)
  res = (C/delta)*(((a-x)./delta).^2.*(x<a)...
        +((x-b)./delta).^2.*(x>b));
end
