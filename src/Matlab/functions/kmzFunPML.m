function res = kmzFunPML(omega,C,delta,ax,bx,ay,by,az,bz,x,y,z)
    kappa = alpha(omega,C,delta,az,bz,z)./(alpha(omega,C,delta,ax,bx,x).*alpha(omega,C,delta,ay,by,y));;
    res = 1./kappa;
    %res = (3+1i)*ones(size(x));
end
