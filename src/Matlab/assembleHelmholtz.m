function [system,solvers] = assembleHelmholtz( T,k,omega )
    kmx = @(x,y,z)kmxFun(x,y,z);
    kmy = @(x,y,z)kmyFun(x,y,z);
    kmz = @(x,y,z)kmzFun(x,y,z);
    c = @(x,y,z)-omega^2*mFun(x,y,z);
    beta1=@(x,y,z) 0.*x;
    beta2=@(x,y,z) 0.*x;
    beta3=@(x,y,z) 0.*x;
    beta={beta1,beta2,beta3};
    f = @(x,y,z)fFun(x,y,z);
    
    Nelts=size(T.elements,1);
    tau=10*ones(4,Nelts);
    
    formulas=checkQuadrature3d(k,1);
    
    [system,solvers] = assembleHDG( kmx,kmy,kmz,c,f,beta,tau,T,k,formulas );
end
