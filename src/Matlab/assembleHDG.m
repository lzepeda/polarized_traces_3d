function [system,solvers] = assembleHDG( kmx,kmy,kmz,c,f,beta,tau,T,k,formulas )
    d2=nchoosek(k+2,2);    
    d3=nchoosek(k+3,3); 
    block3=@(x) (1+(x-1)*d3):(x*d3);
    Nelts =size(T.elements,1);
    Nfaces=size(T.faces,1);
    Ndir  =size(T.dirichlet,1);
    Nneu  =size(T.neumann,1);

    %Matrices for assembly process

    face=int64(T.facebyele)';    % 4 x Nelts
    face=(face(:)-1)*d2;  % First degree of freedom of each face by element                                               
    face=bsxfun(@plus,face,1:d2);    %4*Nelts x d2 (d.o.f. for each face)                                              
    face=reshape(face',4*d2,Nelts);  %d.o.f. for the 4 faces of each element

    [J,I]=meshgrid(1:4*d2);
    R=face(I(:),:); R=reshape(R,4*d2,4*d2,Nelts);
    C=face(J(:),:); C=reshape(C,4*d2,4*d2,Nelts); 
      
    % R_ij^K d.o.f. for local (i,j) d.o.f. in element K ; R_ij^K=C_ji^K
    RowsRHS=reshape(face,4*d2*Nelts,1);

    dirfaces=int64(T.dirfaces(:)-1)*d2;
    dirfaces=bsxfun(@plus,dirfaces,1:d2);
    dirfaces=reshape(dirfaces',d2*Ndir,1);

    free=int64((1:Nfaces)'-1)*d2;
    free=bsxfun(@plus,free,1:d2);
    free=reshape(free',d2*Nfaces,1);
    free(dirfaces)=[];

    neufaces=int64(T.neufaces(:)-1)*d2;
    neufaces=bsxfun(@plus,neufaces,1:d2);
    neufaces=reshape(neufaces',d2*Nneu,1);

    %Local solvers and global system
    [M1,Cf,A1,A2,Af]=localsolvers3dCDVec(kmx,kmy,kmz,c,f,beta,tau,T,k,formulas);
    M=sparse(double(R(:)),double(C(:)),M1(:));
    phif=accumarray(RowsRHS,Cf(:));
    
    system={M,phif,free,dirfaces,neufaces};
    solvers={A1,A2,Af};
end
