# computes the L2 error of the HDG solution uh
# and the function u on the mesh T
using MATLAB
function errorElem( T,u,uh,k )
  # get quadrature points
  @mput k
  @mput T
  @matlab begin
    formulas = checkQuadrature3d(k,0);
    formula = formulas{1};
    x=T.coordinates(:,1); x=formula(:,1:4)*x(transpose(T.elements));
    y=T.coordinates(:,2); y=formula(:,1:4)*y(transpose(T.elements));
    z=T.coordinates(:,3); z=formula(:,1:4)*z(transpose(T.elements));
  end
  @mget x
  @mget y
  @mget z
  @mget formula

  # evaluate the exact solution
  u = u(x,y,z);

  xhat = formula[:,2]
  yhat = formula[:,3]
  zhat = formula[:,4]

  # evaluate the local basis
  @mput xhat
  @mput yhat
  @mput zhat
  @matlab begin
    B=dubiner3d(2*xhat-1,2*yhat-1,2*zhat-1,k);
  end
  @mget B
  
  #compute the approximate solution on each element
  uh=B*uh;

  # compute the error
  err = transpose(formula[:,5])*abs(u-uh).^2*T["volume"]
  sca = transpose(formula[:,5])*abs(u).^2*T["volume"]
  return sqrt(err[1]/sca[1])
end
