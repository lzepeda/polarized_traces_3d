# class model
# in this type we save all the information related to the physics and discretization at
# each layer. In this case we use a simple second order finite difference discretization

include("../src/mesh.jl")
include("../src/helmholtz.jl")

abstract Model

type ModelHDG3D <:Model
  H::SparseMatrixCSC{Complex{Float64},Int64} #sparse matrix
  rhs::Vector{Complex{Float64}}
  mesh
  order
  size

  # local indices
  trace0::Array{Int64,1}
  trace1::Array{Int64,1}
  tracen::Array{Int64,1}
  tracenp::Array{Int64,1}
  volExt::Array{Int64,1}
  volInt::Array{Int64,1}

  # global indices
  trace0Global::Array{Int64,1}
  trace1Global::Array{Int64,1}
  tracenGlobal::Array{Int64,1}
  tracenpGlobal::Array{Int64,1}
  volExtGlobal::Array{Int64,1}
  volIntGlobal::Array{Int64,1}


  freeCutDofsBottom
  freeCutDofsTop

  localSolvers
  free

  solvertype
  Hinv
  

  function ModelHDG3D( layerMesh::LayerMesh, order::Int64, omega::Float64, fac::Float64, delta::Float64, solvertype )
    # assemble the HDG system
    println("    Assemble local system")
    system,localSolvers = assembleHelmholtzPML( layerMesh.mesh, order, omega, fac, delta, layerMesh.ax, layerMesh.bx, layerMesh.ay, layerMesh.by, layerMesh.az, layerMesh.bz );
    
    println("    Find and store trace info")
    # find dofs to be eliminated
    flag = zeros( Bool, size( layerMesh.mesh["faces"],1) );
    flag[ layerMesh.cutFcsBottom ] = true;
    flag[ layerMesh.mesh["dirfaces"] ] = false;
    freeCutFcsBottom = collect(1:length(flag))[flag];
    freeCutDofsBottom = getDofsFromFaces( order, freeCutFcsBottom ); 

    # find dofs to be eliminated
    flag = zeros( Bool, size( layerMesh.mesh["faces"],1) );
    flag[ layerMesh.cutFcsTop ] = true;
    flag[ layerMesh.mesh["dirfaces"] ] = false;
    freeCutFcsTop = collect(1:length(flag))[flag];
    freeCutDofsTop = getDofsFromFaces( order, freeCutFcsTop ); 


    # find free dofs not to be eliminated 
    freeKeptDofs = [];

    # save dofs corresponding to traces, volumen extension and interior volume
    # local indices
    trace0 = getDofsFromFaces( order, layerMesh.trace0Fcs )
    trace1 = getDofsFromFaces( order, layerMesh.trace1Fcs )
    tracen = getDofsFromFaces( order, layerMesh.tracenFcs )
    tracenp = getDofsFromFaces( order, layerMesh.tracenpFcs )
    
    # find free volExt 
    flag = zeros( Bool, size( layerMesh.mesh["faces"],1) );
    flag[ layerMesh.volExtFcs ] = true;
    flag[ layerMesh.mesh["dirfaces"] ] = false;
    volExtFcs = collect(1:length(flag))[flag];
    volExt = getDofsFromFaces( order, volExtFcs );

    # find free volInt 
    flag = zeros( Bool, size( layerMesh.mesh["faces"],1) );
    flag[ round(Int64,layerMesh.volIntFcs) ] = true;
    flag[ round(Int64,layerMesh.mesh["dirfaces"]) ] = false;
    flag[ layerMesh.trace0Fcs ] = false;
    flag[ layerMesh.trace1Fcs ] = false;
    flag[ layerMesh.tracenFcs ] = false;
    flag[ layerMesh.tracenpFcs ] = false;
    volIntFcs = collect(1:length(flag))[flag];
    volInt = getDofsFromFaces( order, volIntFcs );
    cutDofsBottom = freeCutDofsBottom; 
    cutDofsTop = freeCutDofsTop; 

    #global indices
    trace0Global = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ layerMesh.trace0Fcs ] )
    trace1Global = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ layerMesh.trace1Fcs ] )
    tracenGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ layerMesh.tracenFcs ] )
    tracenpGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ layerMesh.tracenpFcs ] )
    volExtGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ volExtFcs ] )
    volIntGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ volIntFcs ] )
    cutDofsBottomGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ freeCutFcsBottom ] )
    cutDofsTopGlobal = getDofsFromFaces( order, layerMesh.localFcToGlobalFc[ freeCutFcsTop ] )

    # compute (reduced) system matrix and rhs
    H = system.mM
    rhs = system.mRhs

    println("    Set up layer model")
    new( H, rhs, layerMesh.mesh,order,size(H,1), trace0, trace1, tracen, tracenp, volExt, volInt, trace0Global, trace1Global, tracenGlobal, tracenpGlobal, volExtGlobal, volIntGlobal, freeCutDofsBottom, freeCutDofsTop, localSolvers, system.mFree, solvertype ); 
  end
end

function extractBoundaryIndices(localModel::Model)
  return (unique(sort(localModel.trace0Global)), unique(sort(localModel.trace1Global)),unique(sort(localModel.tracenGlobal)),unique(sort(localModel.tracenpGlobal)))
end

function extractLocalBoundaryIndices(localModel::Model)
  return (unique(sort(localModel.trace0)), unique(sort(localModel.trace1)),unique(sort(localModel.tracen)),unique(sort(localModel.tracenp)))
end

function extractVolIntIndices(localModel::Model)
  return unique(sort(localModel.volIntGlobal));
end

function extractVolIndices(localModel::Model)
  res = deepcopy( localModel.trace0Global )
  res=vcat( res, localModel.trace1Global )
  res = vcat( res, localModel.tracenGlobal )
  res = vcat( res, localModel.tracenpGlobal )
  res = vcat( res, localModel.volExtGlobal )
  res = vcat( res, localModel.volIntGlobal )

  return unique(sort(res))
end

function extractVolIntLocalIndices(localModel::Model)
  return unique(sort(localModel.volInt));
end

function factorize!(model::Model)
  # factorize!(model::Model)
  # function that performs the LU factorization of the matrix H 
  # within each model. 

  println("Factorizing the local matrix")
  if model.solvertype == "UMFPACK"
    # using built-in umfpack
    model.Hinv = lufact(model.H[ model.free, model.free ] );
  end

  if model.solvertype == "MUMPS"
    # using mumps from Julia Sparse (only shared memory)
    model.Hinv = factorMUMPS(model.H[ model.free, model.free ]);
  end

  if model.solvertype == "MKLPARDISO"
    # using MKLPardiso from Julia Sparse (only shared memory)
    model.Hinv = MKLPardisoSolver();
    set_nprocs(model.Hinv, 16)
    #setting the type of the matrix
    set_mtype(model.Hinv,3)
    # setting we are using a transpose
    set_iparm(model.Hinv,12,2)
    # setting the factoriation phase
    set_phase(model.Hinv, 12)
    X = zeros(Complex128, length( model.free ),1)
    # factorizing the matrix
    pardiso(model.Hinv,X, model.H[ model.free, model.free ],X)
    # setting phase and parameters to solve and transposing the matrix
    # this needs to be done given the different C and Fortran convention
    # used by Pardiso (C convention) and Julia (Fortran Convention)
    set_phase(model.Hinv, 33)
    set_iparm(model.Hinv,12,2)
  end
end

function convert64_32!(model::Model)
  # function to convert the indexing type of an CSC array from 64 bits to 32 bits
  # this functions provides makes the call to MKLPardiso 
  # more efficient, otherwise the conversion is realized at every solve
  if model.solvertype == "MKLPARDISO"
    model.H = SparseMatrixCSC{Complex128,Int32}(model.H)
  else
    println("This method is only to make PARDISO more efficient")
  end
end

function solve(model::Model, f::Array{Complex128,1})
  # u = solve(model::Model, f)
  # function that solves the system Hu=f in the model for one RHS
  # check size
  if (size(f[:])[1] == model.size[1])
    u = zeros( Complex128, model.size );
    temp = deepcopy( u ) 
    if model.solvertype == "UMFPACK"
      u[ model.free ] = model.Hinv\f[model.free];
    end
    # if the linear solver is MUMPS
    if model.solvertype == "MUMPS"
      u[ model.free ] = applyMUMPS(model.Hinv,f[model.free]);
    end
    # if the linear solver is MKL Pardiso
    if model.solvertype == "MKLPARDISO"
      set_phase(model.Hinv, 33)
      tempPar = zeros( Complex128, length( model.free ) )
      pardiso(model.Hinv, tempPar, model.H[ model.free, model.free ], f[ model.free ])
      u[ model.free ] = tempPar;
    end

    return u
  else
    print("The dimensions do not match \n");
    return 0
  end
end

function solve(model::Model, f::Array{Complex128,2})
  # u = solve(model::Model, f)
  # function that solves the system Hu=f in the model
  # for multiple RHS simultaneously
  # check size
  if (size(f)[1] == model.size[1])
    u = zeros( Complex128, model.size, size(f)[2] );
    if model.solvertype == "UMFPACK"
      u[ model.free, : ] = model.Hinv\f[ model.free, : ];
    end
    # if the linear solver is MUMPS
    if model.solvertype == "MUMPS"
      u[ model.free, : ] = applyMUMPS(model.Hinv,f[ model.free, : ]);
    end

    # if the linear solvers is MKL Pardiso
    if model.solvertype == "MKLPARDISO"
      set_phase(model.Hinv, 33)
      temp = zeros( Complex128, length(model.free), size(f)[2] );
      pardiso(model.Hinv, temp, model.H[ model.free, model.free ], f[ model.free, : ])
      u[ model.free, : ] = temp;
    end

    return u
  else
    print("The dimensions do not match \n");
    return 0
  end
end

function getDofsFromFaces( order, faces )
  d2 = binomial(order+2,2); 
  return vec( ones(Int64,d2,1)*vec((faces-1)*d2)'+collect(1:d2)*ones(Int64,1,length(faces)));
end

function constructLayeredModel( nx::Int64, ny::Int64, nz::Int64, npml::Int64, nbuffer::Int64, bdy, h::Float64, fac::Float64, order::Int64, omega::Float64, nLayers::Int64, solvertype )
  delta = npml*h;
  bufferWidth = nbuffer*h;
  ax = bdy[1];
  bx = bdy[1]+nx*h;
  ay = bdy[2];
  by = bdy[2]+ny*h;
  az = bdy[3];
  bz = bdy[3]+nz*h;

  println("Generate global mesh")
  globalMesh = getCubeMesh( ax-delta, bx+delta, ay-delta, by+delta, az-delta, bz+delta, nx+2*npml, ny+2*npml, nz+2*npml );

  println("Generate meshes for layers")
  layerMeshes = constructLayers( globalMesh,ax, bx, ay, by, az, bz, delta, bufferWidth, nLayers );
  
  println("Setting up models for layers")
  res = Any[ ModelHDG3D( layerMeshes[ ii ], order, omega, fac, delta, solvertype ) for ii=1:nLayers ]
  return globalMesh, res
end


function writeVTKFileFaceSet( filename, T, faces )
  faceInfo = T["faces"][ faces,1:3 ];
  facePts = unique(sort(vec( faceInfo )));
  points = T["coordinates"]
  elements = faceInfo;
  nEls = size( elements,1 )
  outfile = open( filename, "w" )

  index = 0
  write( outfile, "# vtk DataFile Version 1.0\n" )
  write( outfile, "Unstructured Grid Example\n" )
  write( outfile, "ASCII\n\n" )

  #write point data
  npts = 3*nEls;
  write( outfile, "DATASET UNSTRUCTURED_GRID\n" )
  write( outfile, "Points $npts float\n" )
  for ii=1:nEls
    for jj=1:3
      xCoord = points[elements[ii,jj],1]
      yCoord = points[elements[ii,jj],2]
      zCoord = points[elements[ii,jj],3]
      write( outfile, "$xCoord $yCoord $zCoord\n" )
    end
  end
  write( outfile, "\n" )

  #write element data
  dataSize = nEls * 4;
  write( outfile, "CELLS $nEls $dataSize\n" )
  index = 0
  for ii=1:nEls
    x1 = (ii-1)*3
    x2 = (ii-1)*3 + 1
    x3 = (ii-1)*3 + 2
    write( outfile, "3 $x1 $x2 $x3\n" )
  end
  write( outfile, "\n" )
  write( outfile, "CELL_TYPES $nEls\n" )
  for ii=1:nEls
    write( outfile, "5\n" )
  end
  write( outfile, "\n" )
  close( outfile)
  return
end

function createGlobalRHS( subdomains )
  sizeGlobal = 0;
  for ii=1:length(subdomains)
    max = maximum( extractVolIndices( subdomains[ ii ].model ) );
    if( sizeGlobal < max )
      sizeGlobal = max;
    end
  end

  rhs = Vector{Complex128}( sizeGlobal );
  for ii=1:length(subdomains)
    rhs[ extractVolIntIndices( subdomains[ ii ].model ) ] = subdomains[ ii ].model.rhs[ extractVolIntLocalIndices( subdomains[ ii ].model ) ];
  end
  return rhs
end
