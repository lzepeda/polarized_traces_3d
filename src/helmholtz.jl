include("mesh.jl")

# a type to store the global linear system
type LinearSystem
  mM          # system matrix
  mRhs        # right-hand side 
  mFree       # free DOFs
  mDirfaces   # faces with Dirichlet BC's
  mNeufaces   # faces with Neumann BC's
end

# a type to store the local solvers for
# static condensation
type Solvers
  mA1
  mA2
  mAf
end

# uses Matlab code to assemble Helmholtz' equation with an HDG system 
#
# Input:
#   mesh  : the mesh on which the system should be assembled
#   order : polynomial degree of basis functions
#   omega : frequency
#   fac   : absorption constant for PML
#   delta : physical PML thickness
#   ax  : the left bound in the x direction
#   bx  : the right bound in the x direction
#   ay  : the left bound in the y direction
#   by  : the right bound in the y direction
#   az  : the left bound in the z direction
#   bz  : the right bound in the z direction
#
# Output:
#   LinearSystem  : the global linear system of the assembled problem
#                   after static condensation
#   Solvers       : the local solvers for static condensation
using MATLAB  
function assembleHelmholtzPML( mesh, order, omega,fac, delta, ax,bx,ay,by,az,bz )
  @matlab clear
  @mput mesh
  @mput order
  @mput fac
  @mput ax
  @mput bx
  @mput ay
  @mput by
  @mput az
  @mput bz
  @mput omega
  @mput delta
  @matlab begin
    addpath(genpath("./Matlab/"))
    system,solvers = assembleHelmholtzPML(mesh,order,omega,fac,delta,ax,bx,ay,by,az,bz);
    
    # Complex matrices have to be transferred to Julia by their
    # real and imaginary part separately
    MReal = real(system{1});
    MImag = imag(system{1});
    rhsReal = real(system{2});
    rhsImag = imag(system{2});
    free = system{3};
    dirfaces = system{4};
    neufaces = system{5};

    A1Real = real(solvers{1});
    A1Imag = imag(solvers{1});
    A2Real = real(solvers{2});
    A2Imag = imag(solvers{2});
    AfReal = real(solvers{3});
    AfImag = imag(solvers{3});
  end

  @mget MReal
  @mget MImag
  M = MReal + im*MImag;
  @mget rhsReal
  @mget rhsImag
  rhs = rhsReal + im*rhsImag;
  @mget free
  @mget dirfaces
  @mget neufaces
  @mget A1Real
  @mget A1Imag
  A1 = A1Real + im*A1Imag;
  @mget A2Real
  @mget A2Imag
  A2 = A2Real + im*A2Imag;
  @mget AfReal
  @mget AfImag
  Af = AfReal + im*AfImag;
  return LinearSystem(M,rhs,Array{Int64,1}(free),Array{Int64,1}(dirfaces), Array{Int64}(neufaces)), Solvers(A1,A2,Af)
end

# uses Matlab code to assemble Helmholtz' equation with an HDG system
# all input information, except mesh and order are defined wtihin
# the MATLAB code
#
# Input:
#   mesh  : the mesh on which the system should be assembled
#   k      : polynomial degree of basis functions
#
# Output:
#   LinearSystem  : the global linear system of the assembled problem
#                   after static condensation
#   Solvers       : the local solvers for static condensation
using MATLAB
function assembleHelmholtz(mesh,k)
  @matlab clear
  @mput mesh
  @mput k
  @matlab begin
    addpath(genpath("./Matlab/"))
    system,solvers = assembleHelmholtz(mesh,k);
    
    MReal = real(system{1});
    MImag = imag(system{1});
    rhsReal = real(system{2});
    rhsImag = imag(system{2});
    free = system{3};
    dirfaces = system{4};
    neufaces = system{5};

    A1Real = real(solvers{1});
    A1Imag = imag(solvers{1});
    A2Real = real(solvers{2});
    A2Imag = imag(solvers{2});
    AfReal = real(solvers{3});
    AfImag = imag(solvers{3});
  end

  @mget MReal
  @mget MImag
  M = MReal + im*MImag;
  @mget rhsReal
  @mget rhsImag
  rhs = rhsReal + im*rhsImag;
  @mget free
  @mget dirfaces
  @mget neufaces
  @mget A1Real
  @mget A1Imag
  A1 = A1Real + im*A1Imag;
  @mget A2Real
  @mget A2Imag
  A2 = A2Real + im*A2Imag;
  @mget AfReal
  @mget AfImag
  Af = AfReal + im*AfImag;
  return LinearSystem(M,rhs,Array{Int64,1}(free),Array{Int64,1}(dirfaces), Array{Int64}(neufaces)), Solvers(A1,A2,Af)
end

# solves the global system stored in LinearSystem
# for homogeneous boundary conditions
#
# Input:
#   T       : the mesh on which the system should be assembled
#   k       : polynomial degree of basis functions
#   system  : the assembled global system
#
# Output:
#   Uhatv : a vector containing the HDG solution
#           uhat defined on the skeleton of the mesh
#
function solveSystemHelmholtz( T,k,system::LinearSystem )
  @matlab clear
  @mput k
  @mput T
  @matlab begin
    d2=nchoosek(k+2,2);    
    Nfaces=size(T.faces,1);
    Nneu  =size(T.neumann,1);
    uhatD,qhatN = BC3d( T,k );
  end
  @mget d2
  @mget Nfaces
  @mget Nneu
  @mget uhatD
  @mget qhatN

  d2 = Int64( d2 )
  Nfaces = Int64( Nfaces )
  Nneu = Int64( Nneu )

  #BC
  Uhatv = zeros( Complex{Float64},d2*Nfaces,1 )

  #RHS
  rhs = zeros( Complex{Float64},d2*Nfaces,1 )
  rhs[ system.mFree ] = system.mRhs[ system.mFree ]

  #solve system for free d.o.f.s
  Uhatv[ system.mFree ] = system.mM[ system.mFree, system.mFree ] \ rhs[ system.mFree ];

  return Uhatv
end

# uses Matlab code to revert the static condensation 
#
# Input:
#   T       : the mesh on which the system should be assembled
#   k       : polynomial degree of basis functions
#   solvers : the local solvers for static condensation
#   Uhat    : the vector of the global solution defined on the
#             skeleton of the mesh
#
# Output:
#   Uh : the global (volume) solution, after the
#        HDG postprocessing procedure
#
using MATLAB
function reconstructSolution( T,k,solvers, Uhat )
  A1 = solvers.mA1
  A2 = solvers.mA2
  Af = solvers.mAf
  @matlab clear
  @mput T
  @mput k
  @mput A1 
  @mput A2 
  @mput Af 
  @mput Uhat
  @matlab begin
    d2=nchoosek(k+2,2);   
    Nfaces=size(T.faces,1);
    Uhat=reshape(Uhat,d2,Nfaces);
    solvers = {A1,A2,Af}
    Uh,Qxh,Qyh,Qzh = reconstructSolution(T,k,solvers,Uhat)
    Uh = postprocessingVec(T,k,Qxh,Qyh,Qzh,Uh);
  end
  @mget Uh
  return Uh
end

# uses Matlab code to evaluate the volume solution 
# at the same parametric points in each element
#
# Input:
#   T       : the mesh on which the system should be assembled
#   Uh      : the reconstructed volume solution
#   k       : polynomial degree of basis functions
#   x       : the parametric points used in each element
#
# Output:
#   the values on each element 
#
function evaluateSol( T, Uh, x, k )
  @mput x
  @mput k
  @matlab begin
    B = dubiner3d( x(:,1), x(:,2), x(:,3),k )
  end
  @mget B
  return B*Uh
end


# writes out the partitioning of the layers in a
# vtk file
#
# Input:
#   filename  : the filename of the vtk file
#   T         : the mesh on which the system should be assembled
#   layerEls  : the elements within each layer
#   cutEls    : the elements cut by the interfaces
#
function writeVTKFileLayers( filename, T, layerEls, cutEls )
  nLayers = length(layerEls);
  points = T["coordinates"]
  elements = Array{Int64,2}(T["elements"])
  nEls = size( elements,1 )
  outfile = open( filename, "w" )

  localPts, localEls = getRefinedEl( 0 )
  index = 0
  write( outfile, "# vtk DataFile Version 1.0\n" )
  write( outfile, "Unstructured Grid Example\n" )
  write( outfile, "ASCII\n\n" )

  #write point data
  nPtsRefined = nEls * size( localPts,1 )
  write( outfile, "DATASET UNSTRUCTURED_GRID\n" )
  write( outfile, "Points $nPtsRefined float\n" )
  for ii=1:nEls
    el = vec( elements[ ii, : ] )
    x1 = vec( points[ el[1], : ] )
    x2 = vec( points[ el[2], : ] )
    x3 = vec( points[ el[3], : ] )
    x4 = vec( points[ el[4], : ] )

    for jj=1:size(localPts,1)
      x = x1 + localPts[ jj, 1 ] * ( x2 - x1 )
      x = x + localPts[ jj, 2 ] * ( x3 - x1 )
      x = x + localPts[ jj, 3 ] * ( x4 - x1 )
      xCoord = x[1]
      yCoord = x[2]
      zCoord = x[3]
      write( outfile, "$xCoord $yCoord $zCoord\n" )
    end
  end
  write( outfile, "\n" )

  #write element data
  nElsRefined = nEls * size(localEls, 1 )
  dataSize = nElsRefined * 5;
  write( outfile, "CELLS $nElsRefined $dataSize\n" )
  index = 0
  for ii=1:nEls
    for jj=1:size(localEls,1)
      x1 = index + Int64(localEls[jj,1])-1
      x2 = index + Int64(localEls[jj,2])-1
      x3 = index + Int64(localEls[jj,3])-1
      x4 = index + Int64(localEls[jj,4])-1
      write( outfile, "4 $x1 $x2 $x3 $x4\n" )
    end
    index += 4 * size( localEls,1 )
  end
  write( outfile, "\n" )
  write( outfile, "CELL_TYPES $nElsRefined\n" )
  for ii=1:nElsRefined
    write( outfile, "10\n" )
  end
  write( outfile, "\n" )

  colors = zeros( nEls )
  println(nEls)
  for ii=1:nLayers
    for jj=1:length(layerEls[ ii ])
      colors[ layerEls[ ii ][ jj ] ] = ii;
    end
  end
  for ii=1:nLayers-1
    for jj=1:length(cutEls[ ii ])
      colors[ cutEls[ ii ][ jj ] ] = nLayers + ii;
    end
  end

  write( outfile, "CELL_DATA $nEls\n" )
  write( outfile, "SCALARS scalars float\n" )
  write( outfile, "LOOKUP_TABLE default\n" )
  for ii=1:nEls
    s = colors[ ii ]
    write( outfile, "$s\n" )
  end
 
  close( outfile)
  return
end

# writes out the global solution as a vtk file 
#
# Input:
#   filename  : the filename of the vtk file
#   T         : the mesh on which the system should be assembled
#   Uh        : the reconstructed global solution
#   k         : polynomial degree used for Uh
#   refLevel  : the level of uniform refinement in each
#               element that is employed to properly
#               show a smooth solution
#
function writeVTKFile( filename, T, Uh, k, refLevel )
  points = T["coordinates"]
  elements = Array{Int64,2}(T["elements"])
  nEls = size( elements,1 )
  outfile = open( filename, "w" )

  localPts, localEls = getRefinedEl( refLevel )
  index = 0
  write( outfile, "# vtk DataFile Version 1.0\n" )
  write( outfile, "Unstructured Grid Example\n" )
  write( outfile, "ASCII\n\n" )

  #write point data
  nPtsRefined = nEls * size( localPts,1 )
  write( outfile, "DATASET UNSTRUCTURED_GRID\n" )
  write( outfile, "Points $nPtsRefined float\n" )
  for ii=1:nEls
    el = vec( elements[ ii, : ] )
    x1 = vec( points[ el[1], : ] )
    x2 = vec( points[ el[2], : ] )
    x3 = vec( points[ el[3], : ] )
    x4 = vec( points[ el[4], : ] )

    for jj=1:size(localPts,1)
      x = x1 + localPts[ jj, 1 ] * ( x2 - x1 )
      x = x + localPts[ jj, 2 ] * ( x3 - x1 )
      x = x + localPts[ jj, 3 ] * ( x4 - x1 )
      xCoord = x[1]
      yCoord = x[2]
      zCoord = x[3]
      write( outfile, "$xCoord $yCoord $zCoord\n" )
    end
  end
  write( outfile, "\n" )

  #write element data
  nElsRefined = nEls * size(localEls, 1 )
  dataSize = nElsRefined * 5;
  write( outfile, "CELLS $nElsRefined $dataSize\n" )
  index = 0
  for ii=1:nEls
    for jj=1:size(localEls,1)
      x1 = index + Int64(localEls[jj,1])-1
      x2 = index + Int64(localEls[jj,2])-1
      x3 = index + Int64(localEls[jj,3])-1
      x4 = index + Int64(localEls[jj,4])-1
      write( outfile, "4 $x1 $x2 $x3 $x4\n" )
    end
    index += 4 * size( localEls,1 )
  end
  write( outfile, "\n" )
  write( outfile, "CELL_TYPES $nElsRefined\n" )
  for ii=1:nElsRefined
    write( outfile, "10\n" )
  end
  write( outfile, "\n" )

  @mput localPts
  @mput k
  @matlab begin
    temp = dubiner3d( -1+2*localPts(:,1), -1+2*localPts(:,2), -1+2*localPts(:,3),k );
  end
  @mget temp
  res = temp * Uh
  write( outfile, "POINT_DATA $nPtsRefined\n" )
  write( outfile, "SCALARS scalars float\n" )
  write( outfile, "LOOKUP_TABLE default\n" )
  for ii=1:nEls
    for jj=1:size(localPts,1)
      s = res[ jj,ii ]
      write( outfile, "$s\n" )
    end
  end
 
  close( outfile)
  return
end

function getRefinedEl( refLevel )
  points = reshape( [0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0],3,4 )
  els = reshape( [ 1,2,3,4 ],4,1 )
  if( refLevel <= 0 )
    return transpose(points), transpose(els)
  end

  refinedPts, refinedEls = getRefinedEl( refLevel-1 )
  nRefinedPts = size( refinedPts, 2 )
  nRefinedEls = size( refinedEls, 2 )

  resPoints = zeros(3,0)
  resEls = zeros(Int64,4,0)

  refinedPts = transpose( refinedPts ) 
  refinedEls = transpose( refinedEls ) 
  #1st element: points 1,5,6,7
  pointsLocal = reshape( [0.5,0.0,0.0],3,1 ) * ones(1,size(refinedPts,2) ) + 0.5 * refinedPts
  elsLocal = refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
  
  #2nd element: points 2,5,8,9
  pointsLocal = reshape( [0.0,0.5,0.0],3,1 ) * ones(1,size(refinedPts,2) ) + 0.5 * refinedPts
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #3rd element: points 3,6,8,10
  pointsLocal = 0.5 * refinedPts
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #4th element: points 4,7,9,10
  pointsLocal = reshape( [0.0,0.0,0.5],3,1 ) * ones(1,size(refinedPts,2) ) + 0.5 * refinedPts
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #5th element: points 5,6,8,9
  x5 = [0.5,0.5,0.0]
  x6 = [0.5,0.0,0.0]
  x8 = [0.0,0.5,0.0]
  x9 = [0.0,0.5,0.5]
  pointsLocal = x5 * ones(1,size(refinedPts,2))  + (x6-x5)*refinedPts[1,:]
  pointsLocal += (x8-x5)*refinedPts[2,:]
  pointsLocal += (x9-x5)*refinedPts[3,:]
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #6th element: points 6,7,9,10
  x6 = [0.5,0.0,0.0]
  x7 = [0.5,0.0,0.5]
  x9 = [0.0,0.5,0.5]
  x10 = [0.0,0.0,0.5]
  pointsLocal = x6 * ones(1,size(refinedPts,2))  + (x7-x6)*refinedPts[1,:]
  pointsLocal += (x9-x6)*refinedPts[2,:]
  pointsLocal += (x10-x6)*refinedPts[3,:]
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #7th element: points 5,6,7,9
  x5 = [0.5,0.5,0.0]
  x6 = [0.5,0.0,0.0]
  x7 = [0.5,0.0,0.5]
  x9 = [0.0,0.5,0.5]
  pointsLocal = x5 * ones(1,size(refinedPts,2))  + (x6-x5)*refinedPts[1,:]
  pointsLocal += (x7-x5)*refinedPts[2,:]
  pointsLocal += (x9-x5)*refinedPts[3,:]
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )
 
  #8th element: points 6,8,9,10
  x6 = [0.5,0.0,0.0]
  x8 = [0.0,0.5,0.0]
  x9 = [0.0,0.5,0.5]
  x10 = [0.0,0.0,0.5]
  pointsLocal = x6 * ones(1,size(refinedPts,2))  + (x8-x6)*refinedPts[1,:]
  pointsLocal += (x9-x6)*refinedPts[2,:]
  pointsLocal += (x10-x6)*refinedPts[3,:]
  elsLocal = size(resEls,2)*4+refinedEls
  resPoints = hcat( resPoints, pointsLocal )
  resEls = hcat( resEls, elsLocal )

  return transpose(resPoints), transpose(resEls)
end
