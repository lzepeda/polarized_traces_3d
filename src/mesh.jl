# a type containing all geometric information
# for one layer
type LayerMesh
  mesh
  
  ax
  bx
  ay
  by
  az
  bz

  trace0Fcs
  trace1Fcs
  tracenFcs
  tracenpFcs
  cutFcsBottom
  cutFcsTop
  volExtFcs
  volIntFcs

  localElToGlobalEl
  globalElToLocalEl

  localFcToGlobalFc
  globalFcToLocalFc
end

# uses Matlab code to generate a regular mesh
# for the cube [ax,bx]x[ay,by]x[az,bz]
#
# Input:
#   ax  : the left bound in the x direction
#   bx  : the right bound in the x direction
#   ay  : the left bound in the y direction
#   by  : the right bound in the y direction
#   az  : the left bound in the z direction
#   bz  : the right bound in the z direction
#   Nx  : the number of elements in the x direction
#   Ny  : the number of elements in the y direction
#   Nz  : the number of elements in the z direction
#
# Output:
#   T   : a dictionary describing the mesh;
#         for details see Matlab code
using MATLAB
function getCubeMesh( ax,bx,ay,by,az,bz, Nx, Ny, Nz )
  @matlab clear
  @mput ax
  @mput bx
  @mput ay
  @mput by
  @mput az
  @mput bz
  @mput Nx
  @mput Ny
  @mput Nz
  @matlab begin
    addpath(genpath("../src/Matlab/"))
    mesh = tetrahedrization(cube( Nx,Ny,Nz ));
  end
  mesh = @mget mesh
  mesh["coordinates"][:,1] = ax + mesh["coordinates"][:,1] * (bx-ax)
  mesh["coordinates"][:,2] = ay + mesh["coordinates"][:,2] * (by-ay)
  mesh["coordinates"][:,3] = az + mesh["coordinates"][:,3] * (bz-az)
  @mput mesh
  @matlab begin
    mesh = HDGgrid3d(mesh,0);
  end
  mesh = @mget mesh
  return mesh
end

# generates a layerMesh from a global Mesh T 
#
# Input:
#   T           : a dictionary describing global mesh
#   ax          : the left bound in the x direction
#   bx          : the right bound in the x direction
#   ay          : the left bound in the y direction
#   by          : the right bound in the y direction
#   az          : the left bound in the z direction
#   bz          : the right bound in the z direction
#   pmlWidth    : the physical width of the PML
#   bufferWidth : the physical width between interfaces
#                 and PML boundaries
#   nLayers     : number of layers to be constructed
#
# Output:
#   layermesh   : an array of LayerMeshes containing all
#                 layer information
#    
function constructLayers( T, ax,bx,ay,by,az,bz,pmlWidth, bufferWidth, nLayers )
  # get point coordinates
  pts = T["coordinates"]
  nPts = size(pts,1)

  # assign each point to a layer
  width = (bz - az)/nLayers
  ptToLayer = zeros(Int64,nPts)
  for ii=1:nPts
    z = pts[ii,3]
    ptToLayer[ii] = floor( (z-az) / width)
    if( ptToLayer[ii] < 0 )
      ptToLayer[ii] = 1
    elseif( ptToLayer[ii] < nLayers )
      ptToLayer[ii] += 1
    else
      ptToLayer[ii] = nLayers
    end
  end

  # find cut elements
  els = T["elements"]
  nEls = size(els,1)
  cutEls = Any[ zeros(Int64,0) for ii=1:nLayers-1 ] 
  layerEls = Any[ zeros(Int64,0) for ii=1:nLayers ]
  for ii=1:nEls
    el = Array{Int64,2}(els[ii,:])
    localPtToLayers = ptToLayer[el]

    testLayerIndex = localPtToLayers[1]
    done = false
    for jj=1:length(localPtToLayers)
      if( localPtToLayers[ jj ] != testLayerIndex )
        if( localPtToLayers[jj] < testLayerIndex )
          push!(cutEls[localPtToLayers[jj]],ii)
        else
          push!(cutEls[testLayerIndex],ii)
        end
        done = true;
        break;
      end
    end
    if( done != true )
      push!( layerEls[ testLayerIndex ],ii );
    end
  end
  for ii=1:nLayers-1
    cutEls[ii] = unique(sort(cutEls[ii]))
  end
  for ii=1:nLayers
    layerEls[ii] = unique(sort(layerEls[ii]))
  end

  #find cut faces and traces
  cutFcs = Any[ zeros(Int64,0) for ii=1:nLayers-1 ] 
  botTrcs = Any[ zeros(Int64,0) for ii=1:nLayers-1 ] 
  topTrcs = Any[ zeros(Int64,0) for ii=1:nLayers-1 ]
  for ii=1:nLayers-1
    localCutEls = cutEls[ ii ]
    for jj=1:length(localCutEls)
      localFaces = Array{Int64,1}(vec(T["facebyele"][localCutEls[jj],:]))
      for kk=1:length(localFaces)
        fcIndex = localFaces[kk]
        fc = Array{Int64,1}(vec(T["faces"][fcIndex,1:end-1]))
        localPtToLayers = ptToLayer[fc]
        testLayerIndex = localPtToLayers[1]
        isDone = false;
        for jj=1:length(localPtToLayers)
          if( localPtToLayers[ jj ] != testLayerIndex )
            if( localPtToLayers[jj] < testLayerIndex )
              push!(cutFcs[localPtToLayers[jj]],fcIndex)
            else
              push!(cutFcs[testLayerIndex],fcIndex)
            end
            isDone = true;
            break;
          end
        end
        if( !isDone )
          if( testLayerIndex == ii )
            push!(botTrcs[ ii ],fcIndex)
          elseif( testLayerIndex == ii+1 )
            push!(topTrcs[ ii ],fcIndex)
          else
            println("ERROR: Invalid geometry!")
            exit(1)
          end
        end
      end
    end
  end
  #sort and delete doubles
  for ii=1:nLayers-1
    cutFcs[ ii ] = unique(sort(cutFcs[ii]))
    botTrcs[ ii ] = unique(sort(botTrcs[ii]))
    topTrcs[ ii ] = unique(sort(topTrcs[ii]))
  end

  # assign each point to an extended layer
  ptToExtLayer = Any[ zeros(Int64,0) for ii=1:nPts ] 
  for ii=1:nPts
    z = pts[ii,3]
    for jj=1:nLayers
      if( az+(jj-1)*width - pmlWidth - bufferWidth < z && z < az +jj*width + pmlWidth + bufferWidth )
        push!(ptToExtLayer[ ii ],jj);
      end
    end
  end

  els = T["elements"]
  nEls = size(els,1)
  extLayers = Any[ zeros(Int64,0) for ii=1:nLayers ]
  for ii=1:nEls
    el = Array{Int64,2}(els[ii,:])
    for jj=1:length(el)
      layers = ptToExtLayer[ el[ jj ] ];
      for kk=1:length(layers)
        push!( extLayers[ layers[kk] ], ii );
      end
    end
  end

  for ii=1:nLayers
    extLayers[ii] = unique( sort( extLayers[ ii ] ) );
  end

  layerMeshes = Array{LayerMesh,1}( nLayers );

  for ii=1:nLayers
    #assign local (and global) indices to elements
    localElToGlobalEl = extLayers[ii];
    globalElToLocalEl = zeros( Int64, nEls );
    globalElToLocalEl[ localElToGlobalEl ] = 1:length(localElToGlobalEl);

    elements = T["elements"][localElToGlobalEl,:];
    points = unique( sort( vec(elements) ) );

    #assign local (and global) indices to points
    localPtToGlobalPt = Array{Int64,1}(points);
    globalPtToLocalPt = zeros( Int64,nPts );
    globalPtToLocalPt[ localPtToGlobalPt ] = 1:length(localPtToGlobalPt);

    coordinates = T["coordinates"][localPtToGlobalPt,:];
    facebyele = T["facebyele"][ localElToGlobalEl,: ];

    #find boundary faces
    boundaryFaces = [];
    sortedFaces = sort( vec(facebyele) );
    index = 1;
    while( index <= length( sortedFaces ) )
      testFace = sortedFaces[index];
      if( index == length(sortedFaces) )
        push!( boundaryFaces, testFace );
        break;
      end
      if( sortedFaces[ index+1 ] != testFace )
        push!( boundaryFaces, testFace );
        index+=1;
      else
        index+=2;
      end
    end
    boundaryFaces = Array{Int64,1}(unique(sort( boundaryFaces )));

    dirfaces = Array{Int64,2}(boundaryFaces');
    faces = unique( sortedFaces );

    #assign local (and global) indices to faces
    localFcToGlobalFc = Array{Int64,1}(faces);
    globalFcToLocalFc = zeros( Int64, size(T["faces"],1));
    globalFcToLocalFc[ localFcToGlobalFc ] = 1:length(faces);

    faces = T["faces"][ localFcToGlobalFc, : ];
    dirichlet = T["faces"][ vec(dirfaces),1:3 ];
    orientation = T["orientation"][ localElToGlobalEl,: ];
    normals = T["normals"][ localElToGlobalEl,: ];
    area = T["area"][ localFcToGlobalFc ];
    volume = T["volume"][ localElToGlobalEl ];
    neumann = zeros(0,3);
    neufaces = zeros(1,0);
    perm = T["perm"][ localElToGlobalEl,: ];

    mesh = Dict();
    mesh["elements"] = Array{Float64,2}(globalPtToLocalPt[ Array{Int64,2}(elements) ]);
    mesh["dirfaces"] = Array{Float64,2}(globalFcToLocalFc[ Array{Int64,2}(dirfaces) ]);
    mesh["dirichlet"] = Array{Float64,2}(globalPtToLocalPt[ Array{Int64,2}(dirichlet) ]);
    mesh["orientation"] = orientation
    mesh["normals"] = normals
    mesh["coordinates"] = coordinates;
    mesh["area"] = area;
    mesh["faces"] = globalPtToLocalPt[ Array{Int64,2}(faces[:,1:3]) ];
    mesh["faces"] = Array{Float64,2}(hcat( mesh["faces"], Array{Int64,1}(faces[:,4]) )); 
    mesh["volume"] = volume;
    mesh["neumann"] = neumann;
    mesh["neufaces"] = neufaces;
    mesh["perm"] = perm;
    mesh["facebyele"] = Array{Float64,2}(globalFcToLocalFc[ Array{Int64,2}(facebyele) ]);

    localTrace0Fcs = [];
    localTrace1Fcs = [];
    localVolCutElsBottom = [];
    localVolCutElsTop = [];
    localCutFcsBottom = [];
    localCutFcsTop = [];
    if( ii > 1 )
      localTrace0Fcs = globalFcToLocalFc[ botTrcs[ ii-1 ] ]; 
      localTrace1Fcs = globalFcToLocalFc[ topTrcs[ ii-1 ] ];
      localVolCutElsBottom = vcat( localVolCutElsBottom, globalElToLocalEl[ cutEls[ ii-1 ] ] );
      localCutFcsBottom = vcat( localCutFcsBottom, globalFcToLocalFc[ cutFcs[ ii-1 ] ] )
    end
    localTracenFcs = [];
    localTracenpFcs = [];
    if( ii < nLayers )
      localTracenFcs = globalFcToLocalFc[ botTrcs[ ii ] ]; 
      localTracenpFcs = globalFcToLocalFc[ topTrcs[ ii ] ];
      localVolCutElsTop = vcat( localVolCutElsTop, globalElToLocalEl[ cutEls[ ii ] ] );
      localCutFcsTop = vcat( localCutFcsTop, globalFcToLocalFc[ cutFcs[ ii ] ] )
    end
    localVolIntEls = globalElToLocalEl[ layerEls[ ii ] ];
    localVolIntEls = unique( sort( localVolIntEls ) )

    localVolIntFcs = unique(sort(vec(mesh["facebyele"][ localVolIntEls,: ])))
    localVolExtEls = globalElToLocalEl[ extLayers[ ii ] ];
    localVolExtFcs = unique(sort(vec(mesh["facebyele"][ localVolExtEls,: ])))
    flag = ones(Bool,length(localVolExtFcs))
    flag[ localVolIntFcs ] = false;
    flag[ localCutFcsTop ] = false;
    flag[ localCutFcsBottom ] = false;
    localVolExtFcs = localVolExtFcs[ flag ];
 
    zMax = maximum( vec( mesh["coordinates"][:,3] ) )
    zMin = minimum( vec( mesh["coordinates"][:,3] ) )
    azLocal = zMin + pmlWidth 
    bzLocal = zMax - pmlWidth 

    layerMeshes[ ii ] = LayerMesh( mesh, ax, bx, ay, by, azLocal, bzLocal, localTrace0Fcs, localTrace1Fcs, localTracenFcs, localTracenpFcs, localCutFcsBottom, localCutFcsTop, localVolExtFcs, localVolIntFcs, localElToGlobalEl, globalElToLocalEl, localFcToGlobalFc, globalFcToLocalFc );
  end
  return layerMeshes;
end
